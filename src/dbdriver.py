import os
from contextlib import contextmanager

import psycopg2
from psycopg2 import Error
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

DEFAULT_DB_NAME = 'postgres'
DB_NAME = 'Users'
USER = 'postgres'
HOST = 'localhost'
PASSWORD = 'password'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


class Database:
    _is_created = False

    def __init__(self):
        self.db_name = DB_NAME
        self.user = USER
        self.host = HOST
        self.password = PASSWORD

        if not Database._is_created:
            try:
                self._init_db()
            except Error:
                pass
            finally:
                Database._is_created = True

    @contextmanager
    def cursor(self, db_name, user, host, password):
        conn = psycopg2.connect(
            dbname=db_name, user=user, host=host, password=password
        )
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()

        yield cur

        cur.close()
        conn.commit()
        conn.close()

    def _init_db(self):
        """Initialize db"""
        init_db_script = os.path.join(BASE_DIR, 'sql_script/init_db.sql')
        with open(init_db_script, 'r') as f:
            sqltext = f.read()
        with self.cursor(
            DEFAULT_DB_NAME, self.user, self.host, self.password
        ) as cur:
            cur.execute(sqltext)
            cur.callproc("init_db", [self.db_name])

    def get_users(self):
        """Get list of all users"""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("get_users", [])
                return cur.fetchall()
        except Exception as e:
            return []

    def get_user(self, user_id):
        """Get user data"""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("get_user", [user_id])
                return cur.fetchall()
        except Exception as e:
            return []

    def add_user(self, user_name, email, status, phone, mobile):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc(
                    "add_user",
                    [user_name, email, status, phone, mobile]
                )
                return True
        except Exception as e:
            return False

    def update_user(self, user_id, user_name, email, status, phone, mobile):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc(
                    "update_user",
                    [user_id, user_name, email, status, phone, mobile]
                )
                return True
        except Exception as e:
            return False

    def delete_user(self, user_id):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("delete_user", [user_id])
                return True
        except Exception as e:
            return False

    def get_user_courses(self, user_id):
        """Get courses for specific user"""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("get_user_courses", [user_id])
                return cur.fetchall()
        except Exception as e:
            return []

    def get_courses(self):
        """Get all courses"""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("get_courses", [])
                return cur.fetchall()
        except Exception as e:
            return []

    def add_course(self, course_name, code):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc(
                    "add_course",
                    [course_name, code]
                )
                return True
        except Exception as e:
            return False

    def delete_course(self, course_id):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("delete_course", [course_id, ])
                return True
        except Exception as e:
            return False

    def update_references(self, user_id, course_list):
        """..."""
        try:
            with self.cursor(
                self.db_name, self.user, self.host, self.password
            ) as cur:
                cur.callproc("update_refs", [user_id, course_list])
                return True
        except Exception as e:
            return False


if __name__ == '__main__':
    d = Database()
